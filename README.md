# Changelog generator
## Goal:
The script reads `git log` and based on `Conventional Commits` specification
generates `CHANGELOG.md` according to semantic versioning.
## Usage:
1. Open your project's .gitlab-ci.yml file.
2. Add the following code to the script section of your .gitlab-ci.yml file.
```
build-zip:
  variables:
    GIT_STRATEGY: clone
    GIT_DEPTH: 0
  script: - curl -O "https://gitlab.com/wiregate-public/changelog-gen/-/raw/main/changelog-gen.sh" | sh
```

3. Save the changes to your .gitlab-ci.yml file.
4. Whenever you push changes to your repository, GitLab will automatically execute the changelog-gen.sh script and generate a changelog file named `CHANGELOG.md`.
